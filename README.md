# Collaboration, collaboratively, Issue 1.3

## Index

* **Giving up the reins**, ginger coons — *Editor's letter*
* **Look at this camel. My camel is amazing**, Loredana Bontempi, Emanuele Bonetti, Morgan Fortems and Thibaut Hofer — *Editor's letter*
* **Cover process**, Morgan Fortems and Thibaut Hofer — *Production colophon*
* *New Releases*
* *Upcoming events*
* **...**, Dave Crossland — *Column*
* **The voice of the shell—in collaboration with my computer**, Eric Schrijver — *Column*
* **RestructWeb**, Annemieke van der Hoek, Timo Klok, Michael van Schaik — *In practice*
* **Breaking into F/LOSS**, Morgan Fortems — *First time*
* **Libre Graphics Meeting 2011** — *Notebook*
* **Isn't Open Clip Art Library handy?** — *Best of SVG*
* **FF3300** — *Showcase*
* **Bash scripts for generative posters**, Lafkon — *Showcase*
* **Open source recipe for organic logos**, Makemake — *Showcase*
* **Digital Dump and Pickpic**, Parcodiyellowstone — *Showcase*
* **&lt;stdin&gt;**, Alexandre Leray and Stéphanie Vilayphiou — *Showcase*
* **Libre Graphics magazine issue 1.4: the Physical, the Digital and the Designer** — *Call for submissions*
* **Managing artist communities: the case for Ubuntu Artists**, Martin Owens — *Feature*
* **Sparkleshare: pleasantly invisible version control**, ginger coons — *Feature*
* **Parallel School—an interview**, Thibaut Hofer — *Interview*
* *Resource list*
* *Glossary*

## Colophon

Collaboration, collaboratively — August 2011  
Issue 1.3, [Libre Graphics Magazine](http://libregraphicsmag.com) 
ISSN: 1925-1416
Guest editors: Loredana Bontempi, Emanuele Bonetti, Morgan Fortems and Thibaut Hofer.

**Editorial Team:**  
* [Ana Isabel Carvalho](http://manufacturaindependente.org)
* [ginger coons](http://adaptstudio.ca)
* [Ricardo Lafuente](http://manufacturaindependente.org)

**Publisher**  
ginger coons

**Community Board:**  
Dave Crossland, Louis Desjardins, Aymeric Mansoux, Alexandre Prokoudine, Femke Snelting

**Contributors:**  
* [Emanuele Bonetti](http://www.parcodiyellowstone.it/it/)
* [Loredana Bontempi](http://www.parcodiyellowstone.it/it/)
* [Dave Crossland](http://understandingfonts.com/who/dave-crossland/)
* [Morgan Fortems](http://www.morganfortems.com)
* [Martin Owens](http://doctormo.deviantart.com/)
* [Eric Schrijver](http://ericschrijver.nl/)
* [Thibaut Hofer](https://twitter.com/calcyum)

Printed in Porto by [Cromotema](http://cromotema.pt/) on recycled paper.

Licensed under a [Creative Commons Attribution-ShareAlike license (CC BY-SA)](https://creativecommons.org/licenses/by-sa/3.0/).  
All content should be attributed to its individual author.  
All content without a stated author can be credited to Libre Graphics Magazine.  

Write to us at enquiries@libregraphicsmag.com  
Our repositories with all source material for the magazine can be found at [https://gitlab.com/libregraphicsmag/vol1issue3](https://gitlab.com/libregraphicsmag/vol1issue3)

## Relevant links

* [Public announcement](http://libregraphicsmag.com/2011/09/finally-issue-1-3-is-out/)
* [PDF Hi-res — download for print (41 MB)](http://libregraphicsmag.com/files/libregraphicsmag_1.3_highquality.pdf)
* [PDF Low-res — download for screen (10 MB)](http://libregraphicsmag.com/files/libregraphicsmag_1.3_lowquality.pdf)
* [Git repository](https://gitlab.com/libregraphicsmag/vol1issue3)
* [Related post](http://libregraphicsmag.com/tag/1-3/) from the magazine blog
